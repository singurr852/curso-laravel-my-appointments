<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateSpecialty;
use App\Specialty;

use App\Http\Controllers\Controller;

class SpecialtyController extends Controller
{

    public function index(){
        $specialties = Specialty::all();
        return view('specialties.index', compact('specialties'));
    }

    public function create(){
        return view('specialties.create');
    }

    public function edit(Specialty $specialty){
        return view('specialties.edit', compact('specialty'));
    }

    public function store(CreateSpecialty $request){
        //dd($request->all());

        $specialty = new Specialty();
        $specialty->name = $request->input('name');
        $specialty->description = $request->input('description');
        $specialty->save();

        $notification = 'La especialidad se ha registrado correctamente.';
        return redirect('/specialties')->with(compact('notification'));
    }

    public function update(CreateSpecialty $request, Specialty $specialty){
        //dd($request->all());

        $specialty->name = $request->input('name');
        $specialty->description = $request->input('description');
        $specialty->save();//UPDATE

        $notification = 'La especialidad se ha actualizado correctamente.';
        return redirect('/specialties')->with(compact('notification'));
    }

    public function destroy(Specialty $specialty){
        $deleteSpecialty = $specialty->name;

        $specialty->delete();
        $notification = 'La especialidad '.$deleteSpecialty.' se ha eliminado correctamente.';
        return redirect('/specialties')->with(compact('notification'));
    }
}
