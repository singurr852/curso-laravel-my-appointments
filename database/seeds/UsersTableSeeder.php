<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Emmanuel Cruz',
            'email' => 'singurr852@gmail.com',
            'password' => bcrypt('slip123123'), // password
            'cedula' => '84286247',
            'address' => '',
            'phone' => '',
            'role' => 'admin',
        ]);
        factory(User::class, 50)->create();
    }
}
